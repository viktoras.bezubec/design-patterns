<?php

namespace DP\Creational\Singleton\Example;

use DateTime;
use DP\Creational\Singleton\Implementation\Singleton;

class LoggerSingleton extends Singleton
{
    private const FILE_NAME = 'singleton_logfile.log';
    private bool $isDisabled = false;

    public function log(string $message): void
    {
        if (!$this->isDisabled) {
            $logFile = self::FILE_NAME;
            $timeStamp = (new DateTime())->format('Y:m:d H:i:s');

            file_put_contents($logFile, "$timeStamp: $message\n", FILE_APPEND);
        } else {
            print "Logging is disabled.\n";
        }
    }

    public function disableLogging(): void
    {
        $this->isDisabled = true;
    }
}
