Singleton pattern ensures a class has only one instance
and provides global point of access to it.

Problems: 

    1. Ensure that a class has just a single instance
    2. Provide a global access point to that instance

Resource:
```
Video https://www.youtube.com/watch?v=hUE_j6q0LTQ&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc&index=7&t=1s
```
