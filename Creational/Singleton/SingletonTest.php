<?php

namespace DP\Creational\Singleton;

use DP\Creational\Singleton\Implementation\Singleton;
use PHPUnit\Framework\TestCase;

class SingletonTest extends TestCase
{
    public function testSingletonInstances(): void
    {
        $firstSingleton = Singleton::getInstance();
        $secondSingleton = Singleton::getInstance();

        $this->assertEquals($firstSingleton, $secondSingleton);
    }

    public function testCreateSingletonWithNewOperator(): void
    {
        $this->expectException(\Error::class);
        $this->expectErrorMessage("Call to protected DP\Creational\Singleton\Implementation\Singleton::__construct() from context 'DP\Creational\Singleton\SingletonTest'");

        // Trying to create Singleton with `new` operator.
        new Singleton();
    }

    public function testCloneSingleton(): void
    {
        $singleton = Singleton::getInstance();

        $this->expectException(\Error::class);
        $this->expectErrorMessage("Call to protected DP\Creational\Singleton\Implementation\Singleton::__clone() from context 'DP\Creational\Singleton\SingletonTest'");

        // Trying to clone Singleton.
        clone $singleton;
    }

    public function testUnserializeSingleton(): void
    {
        $singleton = Singleton::getInstance();

        $this->expectException(\Exception::class);
        $this->expectErrorMessage('Cannot unserialize a Singleton.');

        unserialize(serialize($singleton));
    }
}
