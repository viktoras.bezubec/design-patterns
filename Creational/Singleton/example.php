<?php

require __DIR__.'/../../vendor/autoload.php';

use DP\Creational\Singleton\Example\LoggerSingleton;

print "Testing Singleton pattern.\n";

print "Get first instance.\n";
$logger1 = LoggerSingleton::getInstance();

print "Write first log message.\n";
$logger1->log('First log');

print "Disable logging.\n";
$logger1->disableLogging();

print "Get second instance.\n";
$logger2 = LoggerSingleton::getInstance();

print "Try to log another message.\n";
$logger2->log('This message will not be logged.');
