<?php

namespace DP\Creational\Singleton\Implementation;

class Singleton
{
    /**
     * @var self
     */
    private static $instance;

    /**
     * The Singleton's constructor should always be private to prevent direct
     * construction calls with the `new` operator.
     */
    protected function __construct() {}
    /**
     * Singleton should not be cloneable.
     */
    protected function __clone() {}
    /**
     * Singletons should not be restorable from strings.
     */
    public function __wakeup()
    {
        throw new \Exception('Cannot unserialize a Singleton.');
    }

    public static function getInstance(): self
    {
        if (static::$instance === null) {
            static::$instance = new static;
        }

        return static::$instance;
    }

    /**
     * Business logic
     */
    public function doSomething(): void
    {
        print "Some business logic executed...\n";
    }
}
