<?php

namespace DP\Creational\Prototype\Example;

class Manufacturer
{
    private string $name;
    /**
     * @var array<RobotPrototype>
     */
    private array $robotsReleased;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->robotsReleased = [];
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function addRobot(RobotPrototype $robot): self
    {
        $this->robotsReleased[] = $robot;

        return $this;
    }

    public function getRobotsReleased(): array
    {
        return $this->robotsReleased;
    }
}
