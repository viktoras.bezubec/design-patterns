<?php

namespace DP\Creational\Prototype\Example;

use DateTime;

class RobotPrototype
{
    private string $name;
    private static int $cloneCount = 0;
    private DateTime $creationDate;
    private Manufacturer $manufacturer;
    /** @var array<string> */
    private array $commands;

    private int $legsCount;
    private int $armsCount;
    private int $headsCount;

    public function __construct(string $name, int $legsCount, int $armsCount, int $headsCount, Manufacturer $manufacturer)
    {
        $this->name = $name;
        $this->creationDate = new DateTime();
        $this->commands = [];
        $this->manufacturer = $manufacturer;
        $this->manufacturer->addRobot($this);
        $this->legsCount = $legsCount;
        $this->armsCount = $armsCount;
        $this->headsCount = $headsCount;
    }

    /**
     * You can control what data you want to carry over to the cloned object.
     *
     * For instance, when a robot is cloned:
     * - It gets a new "X clone of the ..." name.
     * - The manufacturer of the robot remains the same. Therefore we leave the
     * reference to the existing object while adding the cloned robot to the list
     * of the manufacturer's released robots.
     * - We don't carry over the commands from the old robot.
     * - We also attach a new date object to the robot.
     */
    public function __clone()
    {
        ++static::$cloneCount;
        $this->name = 'Clone(' . static::$cloneCount . ') of the ' . $this->name; // new name
        $this->creationDate = new DateTime(); // new creation date
        $this->manufacturer->addRobot($this); // add cloned robot to manufacturer
        $this->commands = []; // reset commands
    }

    public function addCommand(string $command): self
    {
        $this->commands[] = $command;

        return $this;
    }

    public function getCommandsCount(): int
    {
        return count($this->commands);
    }

    public function getCreationDate(): DateTime
    {
        return $this->creationDate;
    }

    public function getName(): string
    {
        return '<'.$this->name.'>';
    }

    public function getConstructionDetails(): string
    {
        return sprintf(
            'Legs: %s, arms: %s, heads: %s',
            $this->legsCount,
            $this->armsCount,
            $this->headsCount,
        );
    }
}
