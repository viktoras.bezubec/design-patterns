An object that supports cloning is called a **prototype**

The Prototype pattern is available in PHP out of the box.

Fields containing objects retain their references in a cloned object.

Problem

    Say you have an object, and you want to create an exact copy of it
    
    Your code becomes dependent on that class.
    Copying an object “from the outside” isn’t always possible.
    Sometimes you only know the interface that the object follows, but not its concrete class, when, for example, a parameter in a method accepts any objects that follow some interface.
