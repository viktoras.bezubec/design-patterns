<?php

namespace DP\Creational\Prototype;

use DateTime;
use DP\Creational\Prototype\Implementation\CircularReference;
use DP\Creational\Prototype\Implementation\Prototype;
use PHPUnit\Framework\TestCase;

class PrototypeTest extends TestCase
{
    public function testPrototypePattern(): void
    {
        $prototype = (new Prototype())
            ->setString('random string')
            ->setDateTime(new DateTime());

        $circularReference = new CircularReference($prototype);

        $prototype->setCircularReference($circularReference);

        $clonedPrototype = clone $prototype;

        // String value has been cloned!
        $this->assertEquals($prototype->getString(), $clonedPrototype->getString());
        // DateTime object has been cloned! Not reference to existing object!
        $this->assertFalse($prototype->getDateTime() === $clonedPrototype->getDateTime());
        // Component with back reference has been cloned!
        $this->assertFalse($clonedPrototype->getCircularReference() === $prototype->getCircularReference());
        // Component with back reference is linked to the clone.
        $this->assertFalse($clonedPrototype->getCircularReference()->getPrototype() === $prototype->getCircularReference()->getPrototype());
    }
}
