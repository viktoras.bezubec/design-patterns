<?php

require __DIR__.'/../../vendor/autoload.php';

use DP\Creational\Prototype\Example\Manufacturer;
use DP\Creational\Prototype\Example\RobotPrototype;

$manufacturer = new Manufacturer('Robot factory INC');

$robot = new RobotPrototype('ORIGINAL TWO HEADED ROBOT', 3, 4, 2, $manufacturer);
$robot->addCommand('rm -fr /');

$clonedRobot = clone $robot;

echo "--- Original robot ---\n";
echo "Original robot created: {$robot->getCreationDate()->format('H:i:s.u')}\n";
echo "Original robot name: {$robot->getName()}\n";
echo "Original robot construction: {$robot->getConstructionDetails()}\n";
echo "Original robot commands count: {$robot->getCommandsCount()}\n";

echo "\n";

echo "--- Cloned robot ---\n";
echo "Cloned robot created: {$clonedRobot->getCreationDate()->format('H:i:s.u')}\n";
echo "Cloned robot name: {$clonedRobot->getName()}\n";
echo "Cloned robot construction: {$clonedRobot->getConstructionDetails()}\n";
echo "Cloned robot commands count: {$clonedRobot->getCommandsCount()}\n";

echo "\n";

echo "--- Manufacturer {$manufacturer->getName()} ---\n";
echo 'Robots created: '. count($manufacturer->getRobotsReleased()) . "\n";
echo "First robot created: {$manufacturer->getRobotsReleased()[0]->getName()} ---\n";
echo "Second robot created: {$manufacturer->getRobotsReleased()[1]->getName()} ---\n";
