<?php

namespace DP\Creational\Prototype\Implementation;

use DateTime;

class Prototype
{
    private string $string;
    private DateTime $dateTime;
    private CircularReference $circularReference;

    /**
     * PHP has built-in cloning support. You can `clone` an object without
     * defining any special methods as long as it has fields of primitive types.
     * Fields containing objects retain their references in a cloned object.
     * Therefore, in some cases, you might want to clone those referenced
     * objects as well. You can do this in a special `__clone()` method.
     */
    public function __clone()
    {
        $this->dateTime = clone $this->dateTime;

        $this->circularReference = clone $this->circularReference;
        $this->circularReference->setPrototype($this);
    }

    public function setString(string $string): self
    {
        $this->string = $string;

        return $this;
    }

    public function getString(): string
    {
        return $this->string;
    }

    public function setDateTime(DateTime $dateTime): self
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    public function getDateTime(): DateTime
    {
        return $this->dateTime;
    }

    public function setCircularReference(CircularReference $circularReference): self
    {
        $this->circularReference = $circularReference;

        return $this;
    }

    public function getCircularReference(): CircularReference
    {
        return $this->circularReference;
    }
}
