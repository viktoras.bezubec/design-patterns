<?php

namespace DP\Creational\Prototype\Implementation;

class CircularReference
{
    private Prototype $prototype;

    public function __construct(Prototype $prototype)
    {
        $this->prototype = $prototype;
    }
    public function getPrototype(): Prototype
    {
        return $this->prototype;
    }

    public function setPrototype(Prototype $prototype): CircularReference
    {
        $this->prototype = $prototype;

        return $this;
    }
}
