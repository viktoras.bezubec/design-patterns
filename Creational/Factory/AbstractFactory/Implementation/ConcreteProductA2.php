<?php

namespace DP\Creational\Factory\AbstractFactory\Implementation;

class ConcreteProductA2 implements ProductAInterface
{
    public function getAName(): string
    {
        return 'Product A-2';
    }
}
