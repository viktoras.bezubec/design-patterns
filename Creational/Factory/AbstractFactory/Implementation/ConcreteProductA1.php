<?php

namespace DP\Creational\Factory\AbstractFactory\Implementation;

class ConcreteProductA1 implements ProductAInterface
{
    public function getAName(): string
    {
        return 'Product A-1';
    }
}
