<?php

namespace DP\Creational\Factory\AbstractFactory\Implementation;

class ConcreteProductB1 implements ProductBInterface
{
    public function getBName(): string
    {
        return 'Product B-1';
    }
}
