<?php

namespace DP\Creational\Factory\AbstractFactory\Implementation;

interface ProductBInterface
{
    public function getBName(): string;
}
