<?php

namespace DP\Creational\Factory\AbstractFactory\Implementation;

interface ProductAInterface
{
    public function getAName(): string;
}
