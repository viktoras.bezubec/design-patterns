<?php

namespace DP\Creational\Factory\AbstractFactory\Implementation;

class ConcreteFactoryTwo implements FactoryInterface
{
    public function getProductA(): ProductAInterface
    {
        return new ConcreteProductA2();
    }

    public function getProductB(): ProductBInterface
    {
        return new ConcreteProductB2();
    }
}
