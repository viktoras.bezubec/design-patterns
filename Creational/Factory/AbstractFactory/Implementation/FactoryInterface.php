<?php

namespace DP\Creational\Factory\AbstractFactory\Implementation;

interface FactoryInterface
{
    public function getProductA(): ProductAInterface;

    public function getProductB(): ProductBInterface;
}
