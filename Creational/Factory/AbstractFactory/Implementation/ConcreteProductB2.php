<?php

namespace DP\Creational\Factory\AbstractFactory\Implementation;

class ConcreteProductB2 implements ProductBInterface
{
    public function getBName(): string
    {
        return 'Product B-2';
    }
}
