<?php

namespace DP\Creational\Factory\AbstractFactory\Implementation;

class ConcreteFactoryOne implements FactoryInterface
{
    public function getProductA(): ProductAInterface
    {
        return new ConcreteProductA1();
    }

    public function getProductB(): ProductBInterface
    {
        return new ConcreteProductB1();
    }
}
