### Abstract Factory

Abstract Factory pattern provides an interface for creating families of related or dependent objects
without specifying their concrete classes.

Abstract Factory is a set of Factory methods and makes use of multiple factory methods.

Factory method patterns constructs a single object.
Abstract factory construct multiple objects.

Event if it's technically very simple it's actually massively powerful.
Any factory does not only create a single item but has the capability of producing multiple items of the same family.
