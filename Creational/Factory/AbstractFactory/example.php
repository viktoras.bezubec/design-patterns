<?php

use DP\Creational\Factory\AbstractFactory\Example\BlueThemeFactory;
use DP\Creational\Factory\AbstractFactory\Example\RedThemeFactory;

require __DIR__.'/../../../vendor/autoload.php';

$redTheme = new RedThemeFactory();

$header   = $redTheme->crateHeader();
$body     = $redTheme->createBody();

echo $header->getTitle();
echo $body->getText();

echo "------------------------\n";

$blueTheme = new BlueThemeFactory();

$header   = $blueTheme->crateHeader();
$body     = $blueTheme->createBody();

echo $header->getTitle();
echo $body->getText();
