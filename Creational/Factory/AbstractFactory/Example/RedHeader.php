<?php

namespace DP\Creational\Factory\AbstractFactory\Example;

class RedHeader implements HeaderInterface
{
    private const FONT_COLOR_CODE       = '0;31';
    private const BACKGROUND_COLOR_CODE = '42';

    private string $title;

    public function __construct(string $title)
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return sprintf(
            "\033[%sm\033[%sm%s\033[0m\n",
            self::FONT_COLOR_CODE,
            self::BACKGROUND_COLOR_CODE,
            $this->title
        );
    }
}
