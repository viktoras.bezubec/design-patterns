<?php

namespace DP\Creational\Factory\AbstractFactory\Example;

class RedThemeFactory implements ThemeFactoryInterface
{
    public function crateHeader(): HeaderInterface
    {
        return new RedHeader('This is the red title in the green background.');
    }

    public function createBody(): BodyInterface
    {
        return new RedBody('This is the red text.');
    }
}
