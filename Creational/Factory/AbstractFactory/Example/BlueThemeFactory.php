<?php

namespace DP\Creational\Factory\AbstractFactory\Example;

class BlueThemeFactory implements ThemeFactoryInterface
{
    public function crateHeader(): HeaderInterface
    {
        return new BlueHeader('This is the blue title in the yellow background.');
    }

    public function createBody(): BodyInterface
    {
        return new BlueBody('This is the blue text.');
    }
}
