<?php

namespace DP\Creational\Factory\AbstractFactory\Example;

interface ThemeFactoryInterface
{
    public function crateHeader(): HeaderInterface;

    public function createBody(): BodyInterface;
}
