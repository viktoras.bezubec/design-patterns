<?php

namespace DP\Creational\Factory\AbstractFactory\Example;

interface HeaderInterface
{
    public function getTitle(): string;
}
