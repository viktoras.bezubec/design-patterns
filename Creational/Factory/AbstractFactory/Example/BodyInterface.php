<?php

namespace DP\Creational\Factory\AbstractFactory\Example;

interface BodyInterface
{
    public function getText(): string;
}
