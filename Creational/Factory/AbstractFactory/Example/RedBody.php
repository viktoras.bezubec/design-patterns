<?php

namespace DP\Creational\Factory\AbstractFactory\Example;

class RedBody implements BodyInterface
{
    private const FONT_COLOR_CODE = '0;31';

    private string $text;

    public function __construct(string $text)
    {
        $this->text = $text;
    }

    public function getText(): string
    {
        return sprintf(
            "\033[%sm%s\033[0m\n",
            self::FONT_COLOR_CODE,
            $this->text
        );
    }
}
