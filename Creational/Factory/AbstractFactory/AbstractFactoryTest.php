<?php

namespace DP\Creational\Factory\AbstractFactory;

use DP\Creational\Factory\AbstractFactory\Implementation\ConcreteFactoryOne;
use DP\Creational\Factory\AbstractFactory\Implementation\ConcreteFactoryTwo;
use DP\Creational\Factory\AbstractFactory\Implementation\ConcreteProductA1;
use DP\Creational\Factory\AbstractFactory\Implementation\ConcreteProductA2;
use DP\Creational\Factory\AbstractFactory\Implementation\ConcreteProductB1;
use DP\Creational\Factory\AbstractFactory\Implementation\ConcreteProductB2;
use PHPUnit\Framework\TestCase;

class AbstractFactoryTest extends TestCase
{
    public function testAbstractFactory(): void
    {
        $factoryOne = new ConcreteFactoryOne();
        $this->assertInstanceOf(ConcreteProductA1::class, $factoryOne->getProductA());
        $this->assertInstanceOf(ConcreteProductB1::class, $factoryOne->getProductB());

        $factoryTwo = new ConcreteFactoryTwo();
        $this->assertInstanceOf(ConcreteProductA2::class, $factoryTwo->getProductA());
        $this->assertInstanceOf(ConcreteProductB2::class, $factoryTwo->getProductB());
    }
}
