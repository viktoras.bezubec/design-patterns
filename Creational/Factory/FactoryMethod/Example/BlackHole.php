<?php

namespace DP\Creational\Factory\FactoryMethod\Example;

class BlackHole extends Obstacle
{
    public function __construct(int $size, int $speed)
    {
        $this->name  = 'Black Hole';
        $this->size  = $size;
        $this->speed = $speed;
    }

    public function setPositionX(int $positionX): self
    {
        $this->positionX = $positionX;

        return $this;
    }

    public function setPositionY(int $positionY): self
    {
        $this->positionY = $positionY;

        return $this;
    }
}
