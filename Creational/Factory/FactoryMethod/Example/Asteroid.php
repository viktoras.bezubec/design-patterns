<?php

namespace DP\Creational\Factory\FactoryMethod\Example;

class Asteroid extends Obstacle
{
    public function __construct()
    {
        $this->name = 'Asteroid';
    }

    public function setSize(int $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function setSpeed(int $speed): self
    {
        $this->speed = $speed;

        return $this;
    }

    public function setPositionX(int $positionX): self
    {
        $this->positionX = $positionX;

        return $this;
    }

    public function setPositionY(int $positionY): self
    {
        $this->positionY = $positionY;

        return $this;
    }
}
