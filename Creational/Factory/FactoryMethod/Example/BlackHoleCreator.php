<?php

namespace DP\Creational\Factory\FactoryMethod\Example;

class BlackHoleCreator extends ObstacleCreator
{
    private int $size;
    private int $speed;

    public function __construct(int $size)
    {
        $this->size = $size;
        $this->speed = 0;
    }

    public function createObstacle(): ObstacleInterface
    {
        return (new BlackHole($this->size, $this->speed))
            ->setPositionX(random_int(1, 100))
            ->setPositionY(random_int(1, 100));
    }
}
