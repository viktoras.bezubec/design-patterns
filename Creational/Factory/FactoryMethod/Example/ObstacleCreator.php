<?php

namespace DP\Creational\Factory\FactoryMethod\Example;

abstract class ObstacleCreator
{
    abstract public function createObstacle(): ObstacleInterface;

    public function create(): void
    {
        $obstacle  = $this->createObstacle();

        echo "+--------------------------------------+\n";
        echo "New " . $obstacle->getName() . " created.\n";
        echo sprintf("Position: [%s,%s]\n",
            $obstacle->getPositionX(),
            $obstacle->getPositionY(),
        );
        echo "Size: " . $obstacle->getSize() . ".\n";
        echo sprintf("Difficulty: %s (speed %s).\n",
            $this->determineDifficulty($obstacle->getSpeed()),
            $obstacle->getSpeed()
        );
    }

    private function determineDifficulty(int $speed): string
    {
        switch (true) {
            case $speed < 4:
                return 'EASY';
            case (4 <= $speed && $speed < 55):
                return 'HARD';
            default:
                return 'INSANE';
        }
    }
}
