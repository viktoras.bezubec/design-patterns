<?php

namespace DP\Creational\Factory\FactoryMethod\Example;

class RandomAsteroidCreator extends ObstacleCreator
{
    public function createObstacle(): ObstacleInterface
    {
        return (new Asteroid())
            ->setSize(random_int(1, 4))
            ->setSpeed(random_int(1, 99))
            ->setPositionX(random_int(1, 100))
            ->setPositionY(random_int(1, 100));
    }
}
