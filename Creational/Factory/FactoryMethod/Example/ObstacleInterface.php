<?php

namespace DP\Creational\Factory\FactoryMethod\Example;

interface ObstacleInterface
{
    public function getName(): string;

    public function getSize(): int;

    public function getSpeed(): int;

    public function getPositionX(): int;

    public function getPositionY(): int;
}
