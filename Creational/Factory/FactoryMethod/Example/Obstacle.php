<?php

namespace DP\Creational\Factory\FactoryMethod\Example;

abstract class Obstacle implements ObstacleInterface
{
    protected string $name;
    protected int $size;
    protected int $speed;
    protected int $positionX;
    protected int $positionY;

    public function getName(): string
    {
        return $this->name;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function getSpeed(): int
    {
        return $this->speed;
    }

    public function getPositionX(): int
    {
        return $this->positionX;
    }

    public function getPositionY(): int
    {
        return $this->positionY;
    }
}
