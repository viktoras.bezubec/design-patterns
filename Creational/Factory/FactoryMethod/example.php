<?php

use DP\Creational\Factory\FactoryMethod\Example\BlackHoleCreator;
use DP\Creational\Factory\FactoryMethod\Example\RandomAsteroidCreator;

require __DIR__.'/../../../vendor/autoload.php';

echo "Building SPACE map...\n";
(new RandomAsteroidCreator())->create();
(new RandomAsteroidCreator())->create();
(new RandomAsteroidCreator())->create();
(new BlackHoleCreator(3))->create();
(new BlackHoleCreator(5))->create();
(new RandomAsteroidCreator())->create();
