<?php

namespace DP\Creational\Factory\FactoryMethod\Implementation;

interface FactoryInterface
{
    public function createItem(): ItemInterface;
}
