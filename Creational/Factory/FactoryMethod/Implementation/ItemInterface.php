<?php

namespace DP\Creational\Factory\FactoryMethod\Implementation;

interface ItemInterface
{
    public function operation(): string;
}
