<?php

namespace DP\Creational\Factory\FactoryMethod\Implementation;

class ConcreteItemB implements ItemInterface
{
    public function operation(): string
    {
        return 'B';
    }
}
