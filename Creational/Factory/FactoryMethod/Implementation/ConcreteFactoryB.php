<?php

namespace DP\Creational\Factory\FactoryMethod\Implementation;

class ConcreteFactoryB implements FactoryInterface
{
    public function createItem(): ItemInterface
    {
        return new ConcreteItemB();
    }
}
