<?php

namespace DP\Creational\Factory\FactoryMethod\Implementation;

class ConcreteItemA implements ItemInterface
{
    public function operation(): string
    {
        return 'A';
    }
}
