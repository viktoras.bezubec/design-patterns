<?php

namespace DP\Creational\Factory\FactoryMethod\Implementation;

class ConcreteFactoryA implements FactoryInterface
{
    public function createItem(): ItemInterface
    {
        return new ConcreteItemA();
    }
}
