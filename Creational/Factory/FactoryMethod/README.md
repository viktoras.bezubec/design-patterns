### Factory Method

#### Problem
You want to instantiate an object and encapsulate that instantiation.

Factory is responsible for instantiating it properly and keeping all the object instantiation logic.
Factory can instantiate different objects of the same type, based on business logic.
Factories decide which object to construct and how.

Why to use factory?
* Instantiation is complex and requires some business logic to determine what parameters pass to instantiable object. 
* Polymorphism. If you have a factory, that wraps your construction and that factory is an instance,
you can swap that instance for an instance of another factory.

Factory method pattern defines an interface for creating an object but lets subclasses decide which class to instantiate.
Factory method lets the class defer instantiation to subclasses.
