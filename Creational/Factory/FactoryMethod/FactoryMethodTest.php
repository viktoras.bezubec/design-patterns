<?php

namespace DP\Creational\Factory\FactoryMethod;

use DP\Creational\Factory\FactoryMethod\Implementation\ConcreteFactoryA;
use DP\Creational\Factory\FactoryMethod\Implementation\ConcreteFactoryB;
use DP\Creational\Factory\FactoryMethod\Implementation\ConcreteItemA;
use DP\Creational\Factory\FactoryMethod\Implementation\ConcreteItemB;
use PHPUnit\Framework\TestCase;

class FactoryMethodTest extends TestCase
{
    public function testFactoryMethodPattern(): void
    {
        $itemA = (new ConcreteFactoryA())->createItem();
        $this->assertInstanceOf(ConcreteItemA::class, $itemA);
        $this->assertEquals('A', $itemA->operation());

        $itemB = (new ConcreteFactoryB())->createItem();
        $this->assertInstanceOf(ConcreteItemB::class, $itemB);
        $this->assertEquals('B', $itemB->operation());
    }
}
