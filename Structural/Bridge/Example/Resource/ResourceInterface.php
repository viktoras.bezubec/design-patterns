<?php

declare(strict_types=1);

namespace DP\Structural\Bridge\Example\Resource;

interface ResourceInterface
{
    public function snippet(): string;

    public function title(): string;

    public function image(): string;

    public function url(): string;
}
