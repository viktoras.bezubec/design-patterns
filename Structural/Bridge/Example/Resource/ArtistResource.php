<?php

declare(strict_types=1);

namespace DP\Structural\Bridge\Example\Resource;

class ArtistResource implements ResourceInterface
{
    public function snippet(): string
    {
        return 'Artist snippet';
    }

    public function title(): string
    {
        return 'Artist title';
    }

    public function image(): string
    {
        return 'artist_333.jpg';
    }

    public function url(): string
    {
        return 'http://artist.example.com';
    }
}
