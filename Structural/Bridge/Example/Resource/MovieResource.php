<?php

declare(strict_types=1);

namespace DP\Structural\Bridge\Example\Resource;

class MovieResource implements ResourceInterface
{
    public function snippet(): string
    {
        return 'Movie snippet';
    }

    public function title(): string
    {
        return 'Movie title';
    }

    public function image(): string
    {
        return 'movie_3ff67s.gif';
    }

    public function url(): string
    {
        return 'http://movie.example.com';
    }
}
