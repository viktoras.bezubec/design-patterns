<?php

declare(strict_types=1);

namespace DP\Structural\Bridge\Example\Resource;

class BookResource implements ResourceInterface
{
    public function snippet(): string
    {
        return 'Book snippet';
    }

    public function title(): string
    {
        return 'Book title';
    }

    public function image(): string
    {
        return 'book_asg73k.jpg';
    }

    public function url(): string
    {
        return 'http://book.example.com';
    }
}
