<?php

declare(strict_types=1);

namespace DP\Structural\Bridge\Example\View;

use DP\Structural\Bridge\Example\Resource\ResourceInterface;

abstract class AbstractView
{
    protected ResourceInterface $resource;

    public function __construct(ResourceInterface $resource)
    {
        $this->resource = $resource;
    }

    /**
     * return html representation of view
     */
    abstract public function show(): string;
}
