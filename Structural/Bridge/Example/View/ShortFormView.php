<?php

declare(strict_types=1);

namespace DP\Structural\Bridge\Example\View;

class ShortFormView extends AbstractView
{
    public function show(): string
    {
        return "
            {$this->resource->title()}\n
            {$this->resource->image()}\n
        ";
    }
}
