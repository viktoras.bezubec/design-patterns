<?php

declare(strict_types=1);

namespace DP\Structural\Bridge\Example\View;

class LongFormView extends AbstractView
{
    public function show(): string
    {
        return "
            {$this->resource->title()}\n
            {$this->resource->image()}\n
            {$this->resource->snippet()}\n
            {$this->resource->url()}\n
        ";
    }
}
