<?php

declare(strict_types=1);

namespace DP\Structural\Bridge\Implementation;

/**
 * You can extend the Abstraction without changing the Implementation classes.
 */
class FirstAbstraction extends Abstraction
{
    public function operation(): string
    {
        return "FirstAbstraction: Operation with: {$this->implementation->operationImplementation()}";
    }
}
