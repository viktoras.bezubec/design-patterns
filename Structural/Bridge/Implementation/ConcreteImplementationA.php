<?php

declare(strict_types=1);

namespace DP\Structural\Bridge\Implementation;

/**
 * Each Concrete Implementation corresponds to a specific platform and
 * implements the Implementation interface using that platform's API.
 */
class ConcreteImplementationA implements ImplementationInterface
{
    public function operationImplementation(): string
    {
        return "ConcreteImplementationA: Here's the result on the platform A.";
    }
}
