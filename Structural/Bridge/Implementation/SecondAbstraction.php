<?php

declare(strict_types=1);

namespace DP\Structural\Bridge\Implementation;

class SecondAbstraction extends Abstraction
{
    public function operation(): string
    {
        return "SecondAbstraction: Operation with: {$this->implementation->operationImplementation()}";
    }
}
