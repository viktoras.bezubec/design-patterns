<?php

declare(strict_types=1);

namespace DP\Structural\Bridge\Implementation;

/**
 * The Abstraction defines the interface for the "control" part of the two class
 * hierarchies. It maintains a reference to an object of the Implementation
 * hierarchy and delegates all of the real work to this object.
 */
abstract class Abstraction
{
    protected ImplementationInterface $implementation;

    public function __construct(ImplementationInterface $implementation)
    {
        $this->implementation = $implementation;
    }

    abstract public function operation(): string;
}
