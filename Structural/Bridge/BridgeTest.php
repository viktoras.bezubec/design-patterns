<?php

declare(strict_types=1);

namespace DP\Structural\Bridge;

use DP\Structural\Bridge\Implementation\Abstraction;
use DP\Structural\Bridge\Implementation\ConcreteImplementationA;
use DP\Structural\Bridge\Implementation\ConcreteImplementationB;
use DP\Structural\Bridge\Implementation\FirstAbstraction;
use DP\Structural\Bridge\Implementation\SecondAbstraction;
use PHPUnit\Framework\TestCase;

class BridgeTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testBridgePattern(
        string $abstractionClass,
        string $implementationClass,
        string $expected
    ): void
    {
        /** @var Abstraction $abstraction */
        $abstraction = new $abstractionClass(new $implementationClass());
        $result      = $abstraction->operation();

        self::assertEquals($expected, $result);
    }

    /**
     * @return array[]
     */
    public function dataProvider(): array
    {
        return [
            [FirstAbstraction::class, ConcreteImplementationA::class, "FirstAbstraction: Operation with: ConcreteImplementationA: Here's the result on the platform A."],
            [FirstAbstraction::class, ConcreteImplementationB::class, "FirstAbstraction: Operation with: ConcreteImplementationB: Here's the result on the platform B."],
            [SecondAbstraction::class, ConcreteImplementationA::class, "SecondAbstraction: Operation with: ConcreteImplementationA: Here's the result on the platform A."],
            [SecondAbstraction::class, ConcreteImplementationB::class, "SecondAbstraction: Operation with: ConcreteImplementationB: Here's the result on the platform B."],
        ];
    }
}
