<?php

declare(strict_types=1);

use DP\Structural\Bridge\Example\Resource\ArtistResource;
use DP\Structural\Bridge\Example\Resource\MovieResource;
use DP\Structural\Bridge\Example\View\LongFormView;
use DP\Structural\Bridge\Example\View\ShortFormView;
use DP\Structural\Bridge\Example\View\SnippetFormView;

require __DIR__.'/../../vendor/autoload.php';

echo "Artis long form view -----------------------------------------\n";
$artistLongView = new LongFormView(new ArtistResource());
echo $artistLongView->show();

echo "Artist short form view -----------------------------------------\n";
$artistShortView = new ShortFormView(new ArtistResource());
echo $artistShortView->show();

echo "Movie snippet form view --------------------------------------\n";
$movieSnippetView = new SnippetFormView(new MovieResource());
echo $movieSnippetView->show();
