<?php

namespace DP\Structural\Adapter;

use DP\Structural\Adapter\Implementation\Adaptee;
use DP\Structural\Adapter\Implementation\Adapter;
use DP\Structural\Adapter\Implementation\TargetInterface;
use PHPUnit\Framework\TestCase;

class AdapterTest extends TestCase
{
    public function testAdapterPattern(): void
    {
        $adaptee = new Adaptee();
        $target = new Adapter($adaptee);

        /**
         * The client code supports all classes that follow the Target interface.
         */
        $this->assertInstanceOf(TargetInterface::class, $target);
        $this->expectOutputString('Specific Adaptee request.');
        $target->request();
    }
}
