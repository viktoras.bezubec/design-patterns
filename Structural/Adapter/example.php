<?php

use DP\Structural\Adapter\Example\Client;
use DP\Structural\Adapter\Example\DifferentWriter;
use DP\Structural\Adapter\Example\DifferentWriterAdapter;
use DP\Structural\Adapter\Example\Writer;

require __DIR__.'/../../vendor/autoload.php';

$writer = new Writer();
$differentWriter = new DifferentWriter();

$adapter = new DifferentWriterAdapter($differentWriter);

$firstClient = new Client($writer);
$secondClient = new Client($adapter);

$firstClient->writeMultipleTimes('First word.', 3);
$secondClient->writeMultipleTimes('Second word.', 4);
