The adapter pattern converts the interface of a class into another interface the client expects.
The adapter lets classes work together that couldn't otherwise because of incompatible interfaces.

Adapter pattern is about making 2 interfaces that aren't compatible - compatible.
Adapter is a wrapper.

####Resources
```
https://www.youtube.com/watch?v=2PKQtcJjYvc&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc

https://refactoring.guru/design-patterns/adapter
https://sourcemaking.com/design_patterns/adapter
```
