<?php

namespace DP\Structural\Adapter\Implementation;

/**
 * The Target defines the domain-specific interface used by the client code.
 */
interface TargetInterface
{
    public function request(): void;
}
