<?php

namespace DP\Structural\Adapter\Implementation;

/**
 * The Adapter makes the Adaptee's interface compatible with the Target's interface.
 */
class Adapter implements TargetInterface
{
    private Adaptee $adaptee;

    public function __construct(Adaptee $adaptee)
    {
        $this->adaptee = $adaptee;
    }

    public function request(): void
    {
        $this->adaptee->specificRequest();
    }
}
