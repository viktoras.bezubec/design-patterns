<?php

namespace DP\Structural\Adapter\Example;

class Writer implements WriterInterface
{
    public function write(string $string, int $number): void
    {
        for ($iteration = 0; $iteration < $number; $iteration++) {
            echo $string . "\n";
        }
    }
}
