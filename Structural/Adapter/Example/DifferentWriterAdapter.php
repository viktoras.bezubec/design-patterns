<?php

namespace DP\Structural\Adapter\Example;

class DifferentWriterAdapter implements WriterInterface
{
    private DifferentWriterInterface $differentWriter;

    public function __construct(DifferentWriterInterface $differentWriter)
    {
        $this->differentWriter = $differentWriter;
    }

    public function write(string $string, int $number): void
    {
        $this->differentWriter->writeInDifferentWay($number, $string);
    }
}
