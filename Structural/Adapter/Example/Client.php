<?php

namespace DP\Structural\Adapter\Example;

class Client
{
    private WriterInterface $writer;

    public function __construct(WriterInterface $writer)
    {
        $this->writer = $writer;
    }

    public function writeMultipleTimes(string $word, int $count): void
    {
        $this->writer->write($word, $count);
    }
}
