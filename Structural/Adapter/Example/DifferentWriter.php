<?php

namespace DP\Structural\Adapter\Example;

class DifferentWriter implements DifferentWriterInterface
{
    public function writeInDifferentWay(int $number, string $string): void
    {
        $longString = '';

        for ($iteration = 0; $iteration < $number; $iteration++) {
            $longString .= $string . ' ';
        }

        echo trim($longString) . "\n";
    }
}
