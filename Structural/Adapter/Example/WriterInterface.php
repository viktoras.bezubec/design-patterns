<?php

namespace DP\Structural\Adapter\Example;

interface WriterInterface
{
    public function write(string $string, int $number): void;
}
