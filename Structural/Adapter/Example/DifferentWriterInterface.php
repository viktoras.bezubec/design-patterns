<?php

namespace DP\Structural\Adapter\Example;

interface DifferentWriterInterface
{
    public function writeInDifferentWay(int $number, string $string): void;
}
