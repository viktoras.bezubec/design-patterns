<?php

namespace DP\Structural\Facade\Implementation;

/**
 * The Facade class provides a simple interface to the complex logic of one or
 * several subsystems. The Facade delegates the client requests to the
 * appropriate objects within the subsystem. The Facade is also responsible for
 * managing their lifecycle. All of this shields the client from the undesired
 * complexity of the subsystem.
 */
class Facade
{
    private SubSystemOne $subSystemOne;
    private SubSystemTwo $subSystemTwo;

    /**
     * Depending on your application's needs, you can provide the Facade with
     * existing subsystem objects or force the Facade to create them on its own.
     */
    public function __construct(
        SubSystemOne $subSystemOne,
        SubSystemTwo $subSystemTwo
    )
    {
        $this->subSystemOne = $subSystemOne;
        $this->subSystemTwo = $subSystemTwo;
    }

    /**
     * The Facade's methods are convenient shortcuts to the sophisticated
     * functionality of the subsystems. However, clients get only to a fraction
     * of a subsystem's capabilities.
     */
    public function execute(): string
    {
        $result = $this->subSystemOne->methodOne();
        $result .= $this->subSystemOne->methodN();
        $result .= $this->subSystemTwo->operationOne();
        $result .= $this->subSystemTwo->operationN();

        return $result;
    }
}
