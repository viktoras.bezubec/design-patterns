<?php

namespace DP\Structural\Facade\Implementation;

/**
 * Facade can work with multiple subsystems at the same time.
 */
class SubSystemTwo
{
    public function operationOne(): string
    {
        return 'SubSystem Operation One.';
    }

    // ...

    public function operationN(): string
    {
        return 'SubSystem Operation N.';
    }
}
