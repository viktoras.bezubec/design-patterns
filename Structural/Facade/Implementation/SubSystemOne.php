<?php

namespace DP\Structural\Facade\Implementation;

/**
 * The Subsystem can accept requests either from the facade or client directly.
 * In any case, to the Subsystem, the Facade is yet another client, and it's not
 * a part of the Subsystem.
 */
class SubSystemOne
{
    public function methodOne(): string
    {
        return 'SubSystem Method One.';
    }

    // ...

    public function methodN(): string
    {
        return 'SubSystem Method N.';
    }
}
