####FACADE DESIGN PATTERN
Facade pattern provides a unified interface to a set of interfaces in a subsystem.
Facade defines a higher level interface that makes the subsystem easier to use.

**Use the Facade pattern when you need to have a limited but straightforward interface to a complex subsystem.**

######PROBLEM
You have a bunch of different classes, and you have a bunch of interactions between these different classes.

Facade interacts with all these objects and simplifies interaction for the client.
Client class can use Facade instead of having to directly with all the objects.

The Client uses the facade instead of calling the subsystem objects directly.


######Resources
```
https://www.youtube.com/watch?v=K4FkHVO5iac
https://sourcemaking.com/design_patterns/facade
https://refactoring.guru/design-patterns/facade
```
