<?php

use DP\Structural\Facade\Example\FileUploader;
use DP\Structural\Facade\Example\ImageEditor;
use DP\Structural\Facade\Example\S3Client;

require __DIR__.'/../../vendor/autoload.php';

$filePath = '/var/tmp/image_file.png';

$imageEditor = new ImageEditor();
$s3Client = new S3Client();

$fileUploader = new FileUploader($imageEditor, $s3Client);

$fileUploader->uploadImage($filePath);
