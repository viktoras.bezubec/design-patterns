<?php

namespace DP\Structural\Facade\Example;

class ImageEditor
{
    public function resize(string $imagePath): void { /**... */ }

    public function crop(string $imagePath): void { /**... */ }

    public function blur(string $imagePath): void { /**... */ }
}
