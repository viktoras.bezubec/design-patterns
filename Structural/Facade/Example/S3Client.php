<?php

namespace DP\Structural\Facade\Example;

class S3Client
{
    public function connect(string $token): void { /** */ }

    public function moveFile(string $filePath): void { /** */ }
}
