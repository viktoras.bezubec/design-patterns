<?php

namespace DP\Structural\Facade\Example;

class FileUploader
{
    private ImageEditor $imageEditor;
    private S3Client $s3Client;

    public function __construct(
        ImageEditor $imageEditor,
        S3Client $s3Client
    ) {
        $this->imageEditor = $imageEditor;
        $this->s3Client = $s3Client;
    }

    public function uploadImage(string $imagePath): void
    {
        echo "Resizing image $imagePath ...\n";
        $this->imageEditor->resize($imagePath);

        echo "Cropping image $imagePath ...\n";
        $this->imageEditor->crop($imagePath);

        echo "Blurring image $imagePath ...\n";
        $this->imageEditor->blur($imagePath);

        echo "Initiating connection to AWS S3 ...\n";
        $this->s3Client->connect('ToK3n');

        echo "Moving file $imagePath ...\n";
        $this->s3Client->moveFile($imagePath);
    }

}
