<?php

namespace DP\Structural\Facade;

use DP\Structural\Facade\Implementation\Facade;
use DP\Structural\Facade\Implementation\SubSystemOne;
use DP\Structural\Facade\Implementation\SubSystemTwo;
use PHPUnit\Framework\TestCase;

class FacadeTest extends TestCase
{
    public function testFacadePattern(): void
    {
        $subSystemOne = new SubSystemOne();
        $subSystemTwo = new SubSystemTwo();

        $facade = new Facade($subSystemOne, $subSystemTwo);
        $result = $facade->execute();

        $this->assertEquals(
            'SubSystem Method One.SubSystem Method N.SubSystem Operation One.SubSystem Operation N.',
            $result
        );
    }
}
