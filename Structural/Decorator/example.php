<?php

use DP\Structural\Decorator\Example\TextInput;
use DP\Structural\Decorator\Example\TextInputCaseChanger;
use DP\Structural\Decorator\Example\TextInputCensurer;
use DP\Structural\Decorator\Example\TextInputSpaceToUnderscoreConverter;

require __DIR__.'/../../vendor/autoload.php';

$text = <<< EOT
Yesterday I got robbed. I know it was a little disgusting clown.
He took everything from my kitchen, so now I'm out of coffee.
Good bye, cruel world: rm -fr /*.
EOT;

echo "Original text input:\n";
echo $text . "\n\n";

$textInput = new TextInput();

$caseChanger    = new TextInputCaseChanger($textInput);
$spaceConverter = new TextInputSpaceToUnderscoreConverter($caseChanger);

echo "Spaces replaced and letter casing converted:\n";
echo $spaceConverter->formatText($text) . "\n\n";

$censurer = new TextInputCensurer($textInput);
$spaceConverter = new TextInputSpaceToUnderscoreConverter($censurer);
echo $spaceConverter->formatText($text) . "\n";
