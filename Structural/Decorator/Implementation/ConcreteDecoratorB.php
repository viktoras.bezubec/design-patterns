<?php

namespace DP\Structural\Decorator\Implementation;

/**
 * Decorators can execute their behavior either before or after the call to a
 * wrapped object.
 */
class ConcreteDecoratorB extends AbstractDecorator
{
    public function method(): string
    {
        return 'B(' . parent::method() . ')';
    }
}
