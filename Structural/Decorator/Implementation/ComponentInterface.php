<?php

namespace DP\Structural\Decorator\Implementation;

/**
 * The base Component interface defines operations that can be altered by
 * decorators.
 */
interface ComponentInterface
{
    public function method(): string;
}
