<?php

namespace DP\Structural\Decorator\Implementation;

/**
 * The base Decorator class follows the same interface as the other components.
 * The primary purpose of this class is to define the wrapping interface for all
 * concrete decorators. The default implementation of the wrapping code might
 * include a field for storing a wrapped component and the means to initialize
 * it.
 */
abstract class AbstractDecorator extends Component
{
    protected ComponentInterface $wrapped;

    public function __construct(ComponentInterface $component)
    {
        $this->wrapped = $component;
    }

    /**
     * The Decorator delegates all work to the wrapped component.
     */
    public function method(): string
    {
        return $this->wrapped->method();
    }
}
