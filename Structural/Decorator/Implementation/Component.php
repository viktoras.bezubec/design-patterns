<?php

namespace DP\Structural\Decorator\Implementation;

/**
 * Concrete Components provide default implementations of the operations. There
 * might be several variations of these classes.
 */
class Component implements ComponentInterface
{
    public function method(): string
    {
        return 'Component';
    }
}
