<?php

namespace DP\Structural\Decorator;

use DP\Structural\Decorator\Implementation\Component;
use DP\Structural\Decorator\Implementation\ConcreteDecoratorB;
use DP\Structural\Decorator\Implementation\ConcreteDecoratorA;
use PHPUnit\Framework\TestCase;

class DecoratorTest extends TestCase
{
    /**
     * The client code works with all objects using the Component interface. This
     * way it can stay independent of the concrete classes of components it works
     * with.
     */
    public function testDecoratorPattern(): void
    {
        $component = new Component();
        $this->assertEquals('Component', $component->method());

        $decoratorA = new ConcreteDecoratorA($component);
        $this->assertEquals('A(Component)', $decoratorA->method());

        $decoratorB = new ConcreteDecoratorB($component);
        $this->assertEquals('B(Component)', $decoratorB->method());

        /**
         * Decorators can wrap not only simple components but the other
         * decorators as well.
         */
        $decoratorBA = new ConcreteDecoratorB($decoratorA);
        $this->assertEquals('B(A(Component))', $decoratorBA->method());

        $decoratorAB = new ConcreteDecoratorA($decoratorB);
        $this->assertEquals('A(B(Component))', $decoratorAB->method());

        $decoratorBBAB = new ConcreteDecoratorB(
            new ConcreteDecoratorB(
                new ConcreteDecoratorA(
                    new ConcreteDecoratorB(
                        new Component()
                    )
                )
            )
        );
        $this->assertEquals('B(B(A(B(Component))))', $decoratorBBAB->method());
    }
}
