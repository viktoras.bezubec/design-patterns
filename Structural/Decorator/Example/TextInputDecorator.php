<?php

namespace DP\Structural\Decorator\Example;

abstract class TextInputDecorator implements TextInputInterface
{
    private TextInputInterface $wrapper;

    public function __construct(TextInputInterface $textInput)
    {
        $this->wrapper = $textInput;
    }

    public function formatText(string $text): string
    {
        return $this->wrapper->formatText($text);
    }
}
