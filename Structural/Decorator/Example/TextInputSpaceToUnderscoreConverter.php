<?php

namespace DP\Structural\Decorator\Example;

class TextInputSpaceToUnderscoreConverter extends TextInputDecorator
{
    public function formatText(string $text): string
    {
        $formattedText = parent::formatText($text);

        return str_replace(' ', '_', $formattedText);
    }
}
