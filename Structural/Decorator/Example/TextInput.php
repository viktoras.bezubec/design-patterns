<?php

namespace DP\Structural\Decorator\Example;

class TextInput implements TextInputInterface
{
    public function formatText(string $text): string
    {
        return $text;
    }
}
