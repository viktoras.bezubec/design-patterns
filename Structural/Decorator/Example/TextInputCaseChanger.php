<?php

namespace DP\Structural\Decorator\Example;

class TextInputCaseChanger extends TextInputDecorator
{
    public function formatText(string $text): string
    {
        $formattedText = parent::formatText($text);
        $textLength    = strlen($formattedText);

        for ($i = 0; $i < $textLength; $i++) {
            if (ctype_upper($formattedText[$i])) {
                $formattedText[$i] = mb_strtolower($formattedText[$i]);
            } else {
                $formattedText[$i] = mb_strtoupper($formattedText[$i]);
            }
        }

        return $formattedText;
    }
}
