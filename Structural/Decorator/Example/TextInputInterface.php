<?php

namespace DP\Structural\Decorator\Example;

interface TextInputInterface
{
    public function formatText(string $text): string;
}
