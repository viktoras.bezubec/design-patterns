<?php

namespace DP\Structural\Decorator\Example;

use function PHPUnit\Framework\stringContains;

class TextInputCensurer extends TextInputDecorator
{
    private const SCARY_WORDS = [
        'clown',
        'out of coffee',
        'rm -fr /*'
    ];

    public function formatText(string $text): string
    {
        $formattedText = parent::formatText($text);

        foreach (self::SCARY_WORDS as $scaryWord) {
            if (strpos($formattedText, $scaryWord) !== 0) {
                $formattedText = str_replace($scaryWord, '[*CENSORED*]', $formattedText);
            }
        }

        return $formattedText;
    }
}
