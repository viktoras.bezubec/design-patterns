#### Problem

You want to change the behavior of an object without actually change the object.

#### Solution

* Wrap object into the decorator.

`Decorator can be wrapped into another decorator`

`Decorator is the same type of object`

```
Decorator attaches additional responsibilities to an object dynamically (at runtime)
Decorator provides a flexible alternative to subclassing for extending functionality
```

And remember:
```
Inheritance is not for code reuse!
Inheritance is not for sharing behaviour!
```
