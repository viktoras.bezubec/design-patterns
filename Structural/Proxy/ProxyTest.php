<?php

declare(strict_types=1);

namespace DP\Structural\Proxy;

use DP\Structural\Proxy\Implementation\ProxySubject;
use DP\Structural\Proxy\Implementation\RealSubject;
use PHPUnit\Framework\TestCase;

class ProxyTest extends TestCase
{
    public function testProxyPatternRealSubject(): void
    {
        $realSubject = new RealSubject();

        $this->expectOutputString("RealSubject: Handling request.\n");
        $realSubject->request();
    }

    public function testProxyPatternProxySubject(): void
    {
        $realSubject  = new RealSubject();
        $proxySubject = new ProxySubject($realSubject);

        $expectedOutput  = "Proxy: Checking access prior to firing a real request.\n";
        $expectedOutput .= "RealSubject: Handling request.\n";
        $expectedOutput .= "Proxy: Logging the time of request.\n";

        $this->expectOutputString($expectedOutput);
        $proxySubject->request();
    }
}
