PROXY DESIGN PATTERN

The proxy pattern provides a surrogate or placeholder for another object in order to control access to it.

With this pattern we try to solve a specific set of problems that are all access related.

3 ways (intents/styles) to use a proxy pattern:
1. Remote proxy - when you want to access a resource, that's remote.
2. Virtual proxy - controls access to a resource that is expensive to create.
3. Protection proxy - controls access to a resource based on access rights. Makes sure, that only users,
that are allowed to access the underlying resource do get access to it.

Pattern adds additional behavior with the intent of controlling access to underlying object.
