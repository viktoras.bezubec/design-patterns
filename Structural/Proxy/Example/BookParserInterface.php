<?php

declare(strict_types=1);

namespace DP\Structural\Proxy\Example;

interface BookParserInterface
{
    public function getNumberOfPages(): int;
}
