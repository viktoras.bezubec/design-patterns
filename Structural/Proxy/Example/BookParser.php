<?php

declare(strict_types=1);

namespace DP\Structural\Proxy\Example;

class BookParser implements BookParserInterface
{
    private int $numberOfPages;

    public function __construct(string $bookString)
    {
        // do some huge parsing job
        sleep(5);

        $this->numberOfPages = (int) ceil((strlen($bookString) / 10)) ;
    }

    public function getNumberOfPages(): int
    {
        return $this->numberOfPages;
    }
}
