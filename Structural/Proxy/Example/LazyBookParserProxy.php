<?php

declare(strict_types=1);

namespace DP\Structural\Proxy\Example;

class LazyBookParserProxy implements BookParserInterface
{
    private ?BookParser $bookParser = null;

    private string $bookString;

    public function __construct(string  $bookString)
    {
        $this->bookString = $bookString;
    }

    public function getNumberOfPages(): int
    {
        if ($this->bookParser === null) {
            $this->bookParser = new BookParser($this->bookString);
        }

        return $this->bookParser->getNumberOfPages();
    }
}
