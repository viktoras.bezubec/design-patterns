<?php

use DP\Structural\Proxy\Example\BookParser;
use DP\Structural\Proxy\Example\LazyBookParserProxy;

require __DIR__.'/../../vendor/autoload.php';

$bookString = <<< EOT
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint 
occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
EOT;

// expensive parser initiation
echo "Initiating book parser...\n";
$bookParser = new BookParser($bookString);
echo "Initiation done.\n";
$numberOfPages = $bookParser->getNumberOfPages();
echo "Number of pages: $numberOfPages\n\n";

echo "Initiating lazy proxy...\n";
$proxyParser = new LazyBookParserProxy($bookString);
echo "Initiation done very fast.\n";
echo "Invoking ::getNumberOfPages will take some time, cause BookParser is being initiated.\n";
$numberOfPages = $proxyParser->getNumberOfPages();
echo "Number of pages: $numberOfPages\n";
