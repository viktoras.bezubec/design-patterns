<?php

namespace DP\Behavioral\Observer;

use DP\Behavioral\Observer\Implementation\ObserverOne;
use DP\Behavioral\Observer\Implementation\ObserverTwo;
use DP\Behavioral\Observer\Implementation\Subject;
use PHPUnit\Framework\TestCase;

class ObserverTest extends TestCase
{
    public function testObserverPattern(): void
    {
        $observerOne = new ObserverOne();
        $observerTwo = new ObserverTwo();

        $subject = new Subject();
        $subject->attach($observerOne);
        $subject->attach($observerTwo);

        $subject->setStatus('test status');

        $this->expectOutputString(
            "Observer ONE got new status from subject: test status\n"
            ."Observer TWO got new status from subject: test status\n"
        );
    }
}
