<?php

namespace DP\Behavioral\Observer\Implementation;

use SplObjectStorage;
use SplObserver;
use SplSubject;

class Subject implements SplSubject
{
    /** @var SplObjectStorage|SplObserver[]  */
    private SplObjectStorage $observers;

    private string $status;

    public function __construct()
    {
        $this->observers = new SplObjectStorage();
    }

    public function attach(SplObserver $observer): void
    {
        if (!$this->observers->contains($observer)) {
            $this->observers->attach($observer);
        }
    }

    public function detach(SplObserver $observer): void
    {
        $this->observers->detach($observer);
    }

    public function notify(): void
    {
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;

        $this->notify();
    }
}
