<?php

namespace DP\Behavioral\Observer\Implementation;

use SplObserver;
use SplSubject;

class ObserverOne implements SplObserver
{
    /**
     * @param SplSubject|Subject $subject
     */
    public function update(SplSubject $subject): void
    {
        echo 'Observer ONE got new status from subject: '.$subject->getStatus()."\n";
    }
}
