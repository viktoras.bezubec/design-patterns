<?php

namespace DP\Behavioral\Observer\Implementation;

use SplObserver;
use SplSubject;

class ObserverTwo implements SplObserver
{
    /**
     * @param SplSubject|Subject $subject
     */
    public function update(SplSubject $subject): void
    {
        echo 'Observer TWO got new status from subject: '.$subject->getStatus()."\n";
    }
}
