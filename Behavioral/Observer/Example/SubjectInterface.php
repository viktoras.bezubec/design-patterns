<?php

namespace DP\Behavioral\Observer\Example;

interface SubjectInterface
{
    public function add(ObserverInterface $observer): void;

    public function notify(): void;
}
