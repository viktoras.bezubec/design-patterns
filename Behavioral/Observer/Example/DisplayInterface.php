<?php

namespace DP\Behavioral\Observer\Example;

interface DisplayInterface
{
    public function display(array $rows): void;
}
