<?php

namespace DP\Behavioral\Observer\Example;

class TemperatureDisplay implements ObserverInterface, DisplayInterface
{
    /**
     * @param SubjectInterface|WeatherStation $subject
     */
    public function update(SubjectInterface $subject): void
    {
        $temperature = $subject->getTemperature();

        $this->display([
            "Temperature: $temperature",
        ]);

    }

    public function display(array $rows): void
    {
        echo "<--- Only temperature display --->\n";
        foreach ($rows as $row) {
            echo $row."\n";
        }
        echo "\n";
    }
}
