<?php

namespace DP\Behavioral\Observer\Example;

class MultiDisplay implements ObserverInterface, DisplayInterface
{
    /**
     * @param SubjectInterface|WeatherStation $subject
     */
    public function update(SubjectInterface $subject): void
    {
        $temperature = $subject->getTemperature();
        $humidity = $subject->getHumidity();

        $this->display([
            "Temperature: $temperature C",
            "Humidity: $humidity %",
        ]);
    }

    public function display(array $rows): void
    {
        $displayLength = 30;
        $hrLine = '+'.str_repeat('-', $displayLength - 2).'+';

        echo "<--- Multi display --->\n";
        echo $hrLine."\n";
        foreach ($rows as $row) {
            $rowLength = strlen($row);
            echo '| '.$row.str_repeat(' ', $displayLength - $rowLength - 3)."|\n";
            echo $hrLine."\n";
        }
        echo "\n";
    }
}
