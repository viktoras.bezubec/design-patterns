<?php

namespace DP\Behavioral\Observer\Example;

class WeatherStation implements SubjectInterface
{
    private float $temperature;
    private int $humidity;
    /** @var ObserverInterface[]  */
    private array $observers;

    public function __construct()
    {
        $this->observers = [];
    }

    public function add(ObserverInterface $observer): void
    {
        if (!in_array($observer, $this->observers, true)) {
            $this->observers[] = $observer;
        }
    }

    public function notify(): void
    {
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }

    public function setMetrics(float $temperature, int $humidity): void
    {
        $this->temperature = $temperature;
        $this->humidity = $humidity;

        $this->notify();
    }

    public function getTemperature(): float
    {
        return $this->temperature;
    }

    public function getHumidity(): int
    {
        return $this->humidity;
    }
}
