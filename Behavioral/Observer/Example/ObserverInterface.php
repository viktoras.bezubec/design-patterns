<?php

namespace DP\Behavioral\Observer\Example;

interface ObserverInterface
{
    public function update(SubjectInterface $subject);
}
