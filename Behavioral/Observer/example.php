<?php

require __DIR__.'/../../vendor/autoload.php';

use DP\Behavioral\Observer\Example\WeatherStation;
use DP\Behavioral\Observer\Example\TemperatureDisplay;
use DP\Behavioral\Observer\Example\MultiDisplay;

// create subject
$station = new WeatherStation();

// create observers
$display = new TemperatureDisplay();
$multiDisplay = new MultiDisplay();

$station->add($display);
$station->add($multiDisplay);

$station->setMetrics(34.4, 14);
$station->setMetrics(-13.7, 56);
