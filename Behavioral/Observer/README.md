
####Problem

We have 2 objects, 1st object changes state, second object needs to know when the state of the first object changes.

######Bad solutions:
1. 2nd object can periodically ask does the state of the first object has changed, but this approach is totally inefficient.
What if multiple objects need to know if state of the first object has changed, then everyone needs to call 1st object.
2. 1st object can periodically send messages to the second object, but when there a lot of objects, then 1st object will spam all other objects.
And what if not all objects want to know about 1st object status change.

######Good solution: Observer pattern

Second object is subscriber and subscribes for the 1st object state change.

Observer patterns is push vs poll.
