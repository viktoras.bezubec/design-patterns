<?php

use DP\Behavioral\Command\Example\LightChangeColorCommand;
use DP\Behavioral\Command\Example\LightColored;
use DP\Behavioral\Command\Example\LightMonochrome;
use DP\Behavioral\Command\Example\LightOffCommand;
use DP\Behavioral\Command\Example\LightOnCommand;
use DP\Behavioral\Command\Example\RemoteControl;

require __DIR__.'/../../vendor/autoload.php';

$coloredLight = new LightColored();
$monochromeLight = new LightMonochrome();

$onCommand = new LightOnCommand();
$onCommand
    ->setLight($coloredLight)
    ->setLight($monochromeLight);

$offCommand = new LightOffCommand();
$offCommand
    ->setLight($coloredLight)
    ->setLight($monochromeLight);

$changeColorCommand = new LightChangeColorCommand($coloredLight);

$remoteControl = new RemoteControl($onCommand, $offCommand, $changeColorCommand);

echo "Switching all lights ON.\n";
$remoteControl->clickOn();

echo "\nChange colored light color.\n";
$remoteControl->changeRandomColor();

echo "\nChange colored light color.\n";
$remoteControl->changeRandomColor();

echo "\nChange colored light color.\n";
$remoteControl->changeRandomColor();

echo "\nUndo last color change.\n";
$remoteControl->clickUndoLastAction();

echo "\nTurning off all lights.\n";
$remoteControl->clickOff();

echo "\nUndo last operation.\n";
$remoteControl->clickUndoLastAction();



