####Command pattern
Encapsulate a request as an object thereby letting you parameterize other objects with
different requests, queue or logs requests and support undoable operations.


**Resource**
```
https://www.youtube.com/watch?v=9qA5kw8dcSU
```
