<?php

namespace DP\Behavioral\Command;

use DP\Behavioral\Command\Implementation\ConcreteCommandOne;
use DP\Behavioral\Command\Implementation\Invoker;
use DP\Behavioral\Command\Implementation\Receiver;
use PHPUnit\Framework\TestCase;

class CommandTest extends TestCase
{
    public function testCommandPattern(): void
    {
        $concreteCommandOne = new ConcreteCommandOne(new Receiver());
        $invoker = new Invoker($concreteCommandOne);

        $invoker->startActionOne();
        $invoker->stopActionOne();

        $this->expectOutputString(
            "Starting to do something important. Times to repeat: 1.\n"
        ."Stopping something important.\n");
    }
}
