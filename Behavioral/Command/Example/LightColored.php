<?php

namespace DP\Behavioral\Command\Example;

class LightColored implements LightInterface, LightColoredInterface
{
    private const DEFAULT_COLOR = '0;39';
    private ?string $colorCode = null;

    public function on(): void
    {
        echo sprintf(
            "\033[%sm%s\033[0m\n",
            $this->colorCode ?: self::DEFAULT_COLOR,
            "<<<<(COLOR)>>>>"
        );
    }

    public function off(): void
    {
        echo "    (COLOR)    \n";
    }

    public function changeColor(string $colorCode): void
    {
        $this->colorCode = $colorCode;
        $this->on();
    }
}
