<?php

namespace DP\Behavioral\Command\Example;

class LightOffCommand implements CommandInterface
{
    /**
     * @var LightInterface[]
     */
    private array $lights;

    public function __construct()
    {
        $this->lights = [];
    }

    public function setLight(LightInterface $light): self
    {
        if (!in_array($light, $this->lights, true)) {
            $this->lights[] = $light;
        }

        return $this;
    }

    public function execute(): void
    {
        foreach ($this->lights as $light) {
            $light->off();
        }
    }

    public function unexecute(): void
    {
        foreach ($this->lights as $light) {
            $light->on();
        }
    }
}
