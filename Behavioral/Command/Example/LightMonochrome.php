<?php

namespace DP\Behavioral\Command\Example;

class LightMonochrome implements LightInterface
{
    public function on(): void
    {
        echo "<<<<(MONO)>>>>\n";
    }

    public function off(): void
    {
        echo "    (MONO)    \n";
    }
}
