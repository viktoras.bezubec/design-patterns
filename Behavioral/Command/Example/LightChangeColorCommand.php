<?php

namespace DP\Behavioral\Command\Example;

class LightChangeColorCommand implements CommandInterface
{
    private const COLORS = [
        "0;31", // red
        "0;33", // yellow
        "0;34", // blue
        "0;36", // cyan
        "0;95", // magenta
    ];

    private ?string $lastColor = null;
    private ?string $currentColor = null;
    private LightColoredInterface $light;

    public function __construct(LightColoredInterface $light)
    {
        $this->light = $light;
    }

    public function execute(): void
    {
        $this->lastColor = $this->currentColor;
        $this->currentColor = self::COLORS[array_rand(self::COLORS)];

        $this->light->changeColor($this->currentColor);
    }

    public function unexecute(): void
    {
        if ($this->lastColor) {
            $this->light->changeColor($this->lastColor);
            $this->currentColor = $this->lastColor;
            $this->lastColor = null;
        }
    }
}
