<?php

namespace DP\Behavioral\Command\Example;

class RemoteControl
{
    private CommandInterface $on;
    private CommandInterface $off;
    private CommandInterface $changeColor;

    /**
     * @var CommandInterface[]
     */
    private array $executedCommands;

    public function __construct(CommandInterface $on, CommandInterface $off, CommandInterface $changeColor)
    {
        $this->on = $on;
        $this->off = $off;
        $this->changeColor = $changeColor;

        $this->executedCommands = [];
    }

    public function clickOn(): void
    {
        $this->on->execute();
        $this->executedCommands[] = $this->on;
    }

    public function clickOff(): void
    {
        $this->off->execute();
        $this->executedCommands[] = $this->off;
    }

    public function changeRandomColor()
    {
        $this->changeColor->execute();
        $this->executedCommands[] = $this->changeColor;
    }

    public function clickUndoLastAction(): void
    {
        $lastAction = end($this->executedCommands);

        if ($lastAction) {
            $lastAction->unexecute();

            array_pop($this->executedCommands);
        }
    }
}
