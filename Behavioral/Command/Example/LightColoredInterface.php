<?php

namespace DP\Behavioral\Command\Example;

interface LightColoredInterface
{
    public function changeColor(string $colorCode): void;
}
