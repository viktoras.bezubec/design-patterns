<?php

namespace DP\Behavioral\Command\Example;

interface LightInterface
{
    public function on(): void;

    public function off(): void;
}
