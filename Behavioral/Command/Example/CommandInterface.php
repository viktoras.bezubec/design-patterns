<?php

namespace DP\Behavioral\Command\Example;

interface CommandInterface
{
    public function execute(): void;

    public function unexecute(): void;
}
