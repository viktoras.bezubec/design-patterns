<?php

namespace DP\Behavioral\Command\Implementation;

class ConcreteCommandOne implements CommandInterface
{
    private Receiver $receiver;

    public function __construct(Receiver $receiver)
    {
        $this->receiver = $receiver;
    }

    public function execute(): void
    {
        $this->receiver->doSomething(1);
    }

    public function unexecute(): void
    {
        $this->receiver->undoSomething();
    }
}
