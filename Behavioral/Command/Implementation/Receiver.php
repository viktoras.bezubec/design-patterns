<?php

namespace DP\Behavioral\Command\Implementation;

/**
 * The Receiver class contain some important business logic.
 */
class Receiver
{
    public function doSomething(int $number): void
    {
        echo "Starting to do something important. Times to repeat: $number.\n";
    }

    public function undoSomething(): void
    {
        echo "Stopping something important.\n";
    }
}
