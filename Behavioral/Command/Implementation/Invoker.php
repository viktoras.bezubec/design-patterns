<?php

namespace DP\Behavioral\Command\Implementation;

class Invoker
{
    private CommandInterface $actionOne;

    public function __construct(CommandInterface $actionOne)
    {
        $this->actionOne = $actionOne;
    }

    public function startActionOne(): void
    {
        $this->actionOne->execute();
    }

    public function stopActionOne(): void
    {
        $this->actionOne->unexecute();
    }
}
