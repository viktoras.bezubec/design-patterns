<?php

namespace DP\Behavioral\Command\Implementation;

interface CommandInterface
{
    /**
     * Both methods must take no arguments. Importantly!
     * Because command has itself encapsulated all of the things that need to perform its duties!
     */
    public function execute(): void;

    public function unexecute(): void;
}
