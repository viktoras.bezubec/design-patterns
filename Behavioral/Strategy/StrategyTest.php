<?php

namespace DP\Behavioral\Strategy;

use DP\Behavioral\Strategy\Implementation\ConcreteStrategyOne;
use DP\Behavioral\Strategy\Implementation\ConcreteStrategyTwo;
use DP\Behavioral\Strategy\Implementation\Context;
use DP\Behavioral\Strategy\Implementation\StrategyInterface;
use PHPUnit\Framework\TestCase;

class StrategyTest extends TestCase
{
    /**
     * @dataProvider strategyDataProvider
     */
    public function testConcreteStrategyOne(
        array $integers,
        StrategyInterface $strategy,
        string $expectedResult
    ): void {
        $context = new Context($strategy);
        $result  = $context->execute($integers);

        $this->assertEquals($expectedResult, $result);
    }

    public function strategyDataProvider(): array
    {
        $integers = [200, 45, 234, 3, 1122];

        return [
            [$integers, new ConcreteStrategyOne(), '3->45->200->234->1122'],
            [$integers, new ConcreteStrategyTwo(), '1122<-234<-200<-45<-3'],
        ];
    }

}
