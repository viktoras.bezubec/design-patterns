<?php

namespace DP\Behavioral\Strategy\Implementation;

class ConcreteStrategyTwo implements StrategyInterface
{
    /**
     * reverse sorting
     */
    public function executeBusinessLogic(array $integers): string
    {
        rsort($integers);

        return implode('<-', $integers);
    }
}
