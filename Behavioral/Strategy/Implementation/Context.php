<?php

namespace DP\Behavioral\Strategy\Implementation;

class Context
{
    private StrategyInterface $strategy;

    public function __construct(StrategyInterface $strategy)
    {
        $this->strategy = $strategy;
    }

    public function setStrategy(StrategyInterface $strategy): void
    {
        $this->strategy = $strategy;
    }

    public function execute(array $integers): string
    {
        return $this->strategy->executeBusinessLogic($integers);
    }
}
