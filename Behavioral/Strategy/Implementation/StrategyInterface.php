<?php

namespace DP\Behavioral\Strategy\Implementation;

interface StrategyInterface
{
    public function executeBusinessLogic(array $integers): string;
}
