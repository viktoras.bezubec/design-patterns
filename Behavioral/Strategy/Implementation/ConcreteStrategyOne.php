<?php

namespace DP\Behavioral\Strategy\Implementation;

class ConcreteStrategyOne implements StrategyInterface
{
    /**
     * normal sorting
     */
    public function executeBusinessLogic(array $integers): string
    {
        sort($integers);

        return implode(  '->', $integers);
    }
}
