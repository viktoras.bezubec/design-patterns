<?php

require __DIR__.'/../../vendor/autoload.php';

use DP\Behavioral\Strategy\Example\TextFormatter;

$text = <<< EOT
Lorem Ipsum is simply dummy text of the printing and typesetting industry.
Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
when an unknown printer took a galley of type and scrambled it to make a type
specimen book. It has survived not only five centuries, but also the leap into
electronic typesetting, remaining essentially unchanged. It was popularised
in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
and more recently with desktop publishing software like Aldus PageMaker including
versions of Lorem Ipsum.
EOT;

$contextMixed = new TextFormatter('mixed');
echo $contextMixed->formatText($text)."\n\n";

$contextUpper = new TextFormatter('upper');
echo $contextUpper->formatText($text)."\n\n";

$contextLower = new TextFormatter('lower');
echo $contextLower->formatText($text)."\n\n";
