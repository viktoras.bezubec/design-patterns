Problem

    You have multiple algorithms of the same family.
    You need to decide which algorithm to use, based on client decision.
    
    Put each algorithm into a separate class, and make their objects interchangeable.
