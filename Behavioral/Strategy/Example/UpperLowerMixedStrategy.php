<?php

namespace DP\Behavioral\Strategy\Example;

class UpperLowerMixedStrategy implements TextFormatStrategyInterface
{
    public function format(string $text): string
    {
        $textLength = strlen($text);

        for ($i = 0; $i < $textLength; $i++) {
            if ($i % 2 === 0) {
                $text[$i] = mb_strtoupper($text[$i]);
            } else {
                $text[$i] = mb_strtolower($text[$i]);
            }
        }

        return  $text;
    }
}
