<?php

namespace DP\Behavioral\Strategy\Example;

use Exception;

class TextFormatter
{
    private TextFormatStrategyInterface $strategy;

    public function __construct(string $strategyName)
    {
        $this->strategy = $this->selectStrategy($strategyName);
    }

    public function formatText(string $text): string
    {
        return $this->strategy->format($text);
    }

    private function selectStrategy(string $strategyName): TextFormatStrategyInterface
    {
        switch ($strategyName) {
            case 'upper':
                return new UppercaseStrategy();
            case 'lower':
                return new LowercaseStrategy();
            case 'mixed':
                return new UpperLowerMixedStrategy();
            default:
                throw new Exception('Strategy not implemented');
        }
    }
}
