<?php

namespace DP\Behavioral\Strategy\Example;

class LowercaseStrategy implements TextFormatStrategyInterface
{
    public function format(string $text): string
    {
        return mb_strtolower($text);
    }
}
