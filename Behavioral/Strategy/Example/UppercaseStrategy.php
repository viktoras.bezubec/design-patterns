<?php

namespace DP\Behavioral\Strategy\Example;

class UppercaseStrategy implements TextFormatStrategyInterface
{
    public function format(string $text): string
    {
        return mb_strtoupper($text);
    }
}
