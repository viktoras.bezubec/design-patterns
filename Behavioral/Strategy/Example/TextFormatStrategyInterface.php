<?php

namespace DP\Behavioral\Strategy\Example;

interface TextFormatStrategyInterface
{
    public function format(string $text): string;
}
